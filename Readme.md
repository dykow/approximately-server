### Approximately-server

backend for beacon location services system.

In order to run:
- docker
- docker-compose

Get docker-compose.yml from `installation/docker-compose.yml` 

Run it with:
```docker-compose up -d```
