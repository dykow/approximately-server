package approximately.server.controller.mobile;

import approximately.server.model.ApproxService;
import approximately.server.model.ApproxOrder;
import approximately.server.model.Product;
import approximately.server.model.User;
import approximately.server.repository.ApproxServiceRepository;
import approximately.server.repository.UserRepository;
import approximately.server.service.OrderService;
import approximately.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping("/api/v1")
public class MobileController {

    private ApproxServiceRepository approxServiceRepository;
    private UserService userService;
    private UserRepository userRepository;
    private OrderService orderService;

    @Autowired
    public MobileController(ApproxServiceRepository approxServiceRepository, UserService userService, UserRepository userRepository, OrderService orderService) {
        this.approxServiceRepository = approxServiceRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.orderService = orderService;
    }

    @GetMapping(path = "/user")
    public Optional<User> getUser(Principal p) {
        String email  = getPrincipalEmail(p);
        Optional<User> user = userService.findByEmail(email);
        if (user.isPresent()) {
            user.get().setPassword("");
        }

        return user;
    }

    @GetMapping(path = "/scanned/list")
    public HashSet<ApproxService> getServicesByBeaconUuids(@RequestBody List<UUID> uuids) {
        HashSet<ApproxService> services = new HashSet<>();
        for (UUID uuid : uuids) {
            Optional<ApproxService> as = approxServiceRepository.findByBeaconsUuid(uuid);
            if (as.isPresent()) {
                services.add(as.get());
            }
        }
        return services;
    }

    @GetMapping(path = "/scanned/{beacon_uuid}")
    public Optional<ApproxService> getServiceByBeaconUuid(@PathVariable(name = "beacon_uuid") UUID uuid) {
        return approxServiceRepository.findByBeaconsUuid(uuid);
    }

    @GetMapping(path = "/product/all/service/{service_name}")
    public List<Product> getAllProductsOfService(@PathVariable(name = "service_name") String serviceName) {
        return approxServiceRepository.findById(serviceName).get().getProducts();
    }

    @GetMapping(path = "/history/service/all/{service_name}")
    public List<ApproxOrder> getOrderHistoryOfService(@PathVariable(name = "service_name") String serviceName,
                                                      Principal p) {
        String email = getPrincipalEmail(p);
        User u = userRepository.findById(email).get();
        ArrayList<ApproxOrder> orders = new ArrayList<>();
        for (ApproxOrder o : u.getOrders()) {
            if (o.getService().getName().equals(serviceName))
                orders.add(o);
        }
        return orders;
    }

    @GetMapping(path = "/history/order/one/{order_id}")
    public ApproxOrder getOrder(@PathVariable(name = "order_id") Long orderId,
                                          Principal p) {
        String email = getPrincipalEmail(p);
        User u = userRepository.findById(email).get();
        for (ApproxOrder o : u.getOrders()) {
            if (o.getId() == orderId)
                return o;
        }

        ApproxOrder ao = new ApproxOrder();
        ao.setId(null);
        return ao;
    }

    @GetMapping(path = "/history/order/all")
    public List<ApproxOrder> getAllOrders(Principal p) {
        String email = getPrincipalEmail(p);
        return userRepository.findByEmail(email).get().getOrders();
    }

    @PostMapping(path = "/order")
    public void placeOrder(@RequestBody ApproxOrder order,
                           Principal p) {
        String email = getPrincipalEmail(p);
        orderService.placeOrder(order, email);
    }

    @PostMapping(path = "/balance/add")
    public void addBalance(@RequestBody Long newCharge,
                           Principal p) {
        String email = getPrincipalEmail(p);
        User u = userRepository.findByEmail(email).get();
        u.setBalance(u.getBalance() + newCharge);
        userRepository.save(u);
    }

    private String getPrincipalEmail(Principal p) {
        return p.getName();
    }
}
