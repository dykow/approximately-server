package approximately.server.controller.management;

import approximately.server.model.ApproxService;
import approximately.server.service.ApproxServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/manage")
public class ApproxServiceController {

    private ApproxServiceService approxServiceService;

    @Autowired
    public ApproxServiceController(ApproxServiceService approxServiceService) {
        this.approxServiceService = approxServiceService;
    }

    @GetMapping(path = "/service/{service_name}")
    public Optional<ApproxService> getService(@PathVariable(name = "service_name") String serviceName) {
        Optional<ApproxService> service = approxServiceService.findByName(serviceName);
        return service;
    }

    @PostMapping(path = "/service")
    public void addService(@RequestBody ApproxService approxService) {
        approxServiceService.save(approxService);
    }

    @DeleteMapping(path = "/service/{service_name}")
    public void deleteService(@PathVariable(name = "service_name") String serviceName) {
        approxServiceService.deleteByName(serviceName);
    }
}
