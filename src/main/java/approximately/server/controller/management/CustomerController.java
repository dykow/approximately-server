package approximately.server.controller.management;

import approximately.server.model.Customer;
import approximately.server.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController()
@RequestMapping(path = "/manage")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping(path = "/customer")
    public void addCustomer(@Valid @RequestBody Customer customer) {
        customerService.save(customer);
    }

    @GetMapping(path = "/customer/{customer_name}")
    public Optional<Customer> getCustomer(@PathVariable(name = "customer_name") String customerName) {
        return customerService.findByName(customerName);
    }

    @DeleteMapping(path = "/customer/{customer_name}")
    public void deleteCustomer(@PathVariable(name = "customer_name") String customerName) {
        customerService.deleteByName(customerName);
    }
}
