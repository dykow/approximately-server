package approximately.server.controller.management;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/status")
public class StatusController {

    @GetMapping
    public Map<String, String> getStatus() {
        Map<String, String> status = new HashMap<>(1);
        status.put("status", "active");
        return status;
    }
}
