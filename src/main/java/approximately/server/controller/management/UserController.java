package approximately.server.controller.management;

import approximately.server.model.User;
import approximately.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/manage")
public class UserController {

    UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/user/{email}")
    public Optional<User> getUser(@PathVariable(name = "email") String email) {
        Optional<User> user = userService.findByEmail(email);
        if (user.isPresent()) {
            user.get().setPassword("");
        }

        return user;
    }

    @PostMapping(path = "/user")
    public void addUser(@RequestBody User user) {
        userService.save(user);
    }

    @DeleteMapping(path = "/user/{email}")
    public void deleteUser(@PathVariable(name = "email") String email) {
        userService.deleteById(email);
    }
}
