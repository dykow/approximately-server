package approximately.server.controller.management;

import approximately.server.model.Beacon;
import approximately.server.service.BeaconService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController()
@RequestMapping(path = "/manage")
public class BeaconController {

    BeaconService beaconService;

    @Autowired
    public BeaconController(BeaconService beaconService) {
        this.beaconService = beaconService;
    }

    @GetMapping(path = "/beacon/{beacon_uuid}")
    public Optional<Beacon> getBeacon(@PathVariable(name = "beacon_uuid") UUID beaconUuid) {
        return beaconService.findByUuid(beaconUuid);
    }

    @PostMapping(path = "/beacon")
    public void addBeacon(@RequestBody Beacon beacon) {
        beaconService.save(beacon);
    }

    @DeleteMapping(path = "/beacon/{beacon_uuid}")
    public void deleteBeacon(@PathVariable(name = "beacon_uuid") UUID beaconUuid)  {
        beaconService.deleteByUuid(beaconUuid);
    }
}
