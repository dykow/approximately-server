package approximately.server.controller.management;

import approximately.server.model.Product;
import approximately.server.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/manage")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(path = "/product/{product_id}")
    public Optional<Product> getProduct(@PathVariable(name = "product_id") Long id) {
        return productService.findById(id);
    }

    @PostMapping(path = "/product")
    public void addProduct(@RequestBody Product p) {
        productService.save(p);
    }

    @DeleteMapping(path = "/product/{product_id}")
    public void deleteBeacon(@PathVariable(name = "product_id") Long id)  {
        productService.deleteById(id);
    }
}
