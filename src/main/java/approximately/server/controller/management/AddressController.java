package approximately.server.controller.management;

import approximately.server.model.Address;
import approximately.server.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/manage")
public class AddressController {

    AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping(path = "/address/{id}")
    public Optional<Address> getAddress(@PathVariable Long id) {
        return addressService.findById(id);
    }

    @PostMapping(path = "/address")
    public void addAddress(@RequestBody Address address) {
        addressService.save(address);
    }

    @DeleteMapping(path = "/address/{id}")
    public void deleteAddress(@PathVariable Long id) {
        addressService.deleteById(id);
    }
}
