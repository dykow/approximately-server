package approximately.server.config;

import approximately.server.model.User;
import approximately.server.service.UserService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class SuperUserInitializingBean implements InitializingBean {

    UserService userService;

    public SuperUserInitializingBean(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (userService.findByEmail("admin@mail.com").isEmpty()) {
            User u = new User();
            u.setEmail("admin@mail.com");
            u.setFirstName("Admin");
            u.setSureName("Admin");
            u.setPassword("pass");
            u.setBalance(999999L);
            u.setActive(true);
            u.setRoles("ADMIN");
            userService.save(u);
        }
    }
}
