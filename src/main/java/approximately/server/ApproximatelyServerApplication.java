package approximately.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class ApproximatelyServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApproximatelyServerApplication.class, args);
	}
}
