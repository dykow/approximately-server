package approximately.server.repository;

import approximately.server.model.ApproxService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ApproxServiceRepository extends JpaRepository<ApproxService, String> {

    Optional<ApproxService> findByName(String name);
    void deleteByName(String name);
    Optional<ApproxService> findByBeaconsUuid(UUID uuid);
}
