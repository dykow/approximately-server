package approximately.server.repository;

import approximately.server.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, String> {

    ArrayList<Customer> findAllByName(String name);
    Optional<Customer> findByName(String name);
    void deleteByName(String name);
}
