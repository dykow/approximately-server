package approximately.server.repository;

import approximately.server.model.Beacon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


public interface BeaconRepository extends JpaRepository<Beacon, String> {

    Optional<Beacon> findByUuid(UUID uuid);
    List<Beacon> findAllByUuid(UUID beaconUuid);
    void deleteByUuid(UUID beaconUuid);
}
