package approximately.server.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Product extends AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    private String name = new String();

    private String description = new String();

    @NotBlank
    private Long price = 999999L;

    private String imgUrl = new String();

    public void deepCopy(Product p) {
        setName(p.getName());
        setDescription(p.getDescription());
        setPrice(p.getPrice());
        setImgUrl(p.getImgUrl());
    }
}
