package approximately.server.model;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "approx_user")
public class User extends AbstractEntity<String> {

    @Id
    private String email = new String();

    @NotBlank
    private String password = new String();

    @NotNull
    private Boolean active;

    @NotBlank
    private String roles = new String();

    @NotBlank
    private String firstName = new String();

    @NotBlank
    private String sureName = new String();

    @NotNull
    private Long balance = 0L;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ApproxOrder> orders = new ArrayList<>();

    @Override
    public String getId() {
        return email;
    }
}