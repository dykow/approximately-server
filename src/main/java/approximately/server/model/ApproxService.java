package approximately.server.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class ApproxService extends AbstractEntity<String> {

    @Id
    private String name = new String();

    private String description = new String();

    private String imgUrl = new String();

    @OneToMany(targetEntity = Product.class, cascade = CascadeType.ALL)
    private List<Product> products;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "service_address")
    private Address address =  new Address();

    @OneToMany(cascade = CascadeType.ALL)
    private List<Beacon> beacons = new ArrayList<>();

    @Override
    public String getId() {
        return name;
    }
}
