package approximately.server.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class ApproxOrder extends AbstractEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    private ApproxService service;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "order_products")
    private List<Product> products;

    @CreationTimestamp
    private LocalDateTime creationTimestamp;
}
