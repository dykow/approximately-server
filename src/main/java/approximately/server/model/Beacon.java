package approximately.server.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Beacon extends AbstractEntity<UUID> {

    @Id
    @Column(length=16)
//    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "UUID")
//    @GenericGenerator(
//            name = "UUID",
//            strategy = "org.hibernate.id.UUIDGenerator"
//    )
    private UUID uuid = UUID.randomUUID();

    @Override
    public UUID getId() {
        return uuid;
    }
}
