package approximately.server.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Customer extends AbstractEntity<String> {

    @Id
    @NotBlank
    private String name;

    @ManyToOne(targetEntity = Address.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_address")
    private Address headquarterAdr = new Address();

    @OneToMany(targetEntity = ApproxService.class, cascade = CascadeType.ALL)
    private List<ApproxService> approxServices = new ArrayList<>();

    @Override
    public String getId() {
        return name;
    }
}
