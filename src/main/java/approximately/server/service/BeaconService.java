package approximately.server.service;

import approximately.server.model.Beacon;
import approximately.server.repository.BeaconRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class BeaconService {

    private BeaconRepository beaconRepository;

    @Autowired
    public BeaconService(BeaconRepository beaconRepository) {
        this.beaconRepository = beaconRepository;
    }

    public void save(Beacon beacon) {
        beaconRepository.save(beacon);
    }

    public Optional<Beacon> findByUuid(UUID beaconUuid) {
        return beaconRepository.findByUuid(beaconUuid);
    }

    public void deleteByUuid(UUID beaconUuid) {
        beaconRepository.deleteByUuid(beaconUuid);
    }
}
