package approximately.server.service;

import approximately.server.model.ApproxService;
import approximately.server.repository.ApproxServiceRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ApproxServiceService {

    private ApproxServiceRepository approxServiceRepository;

    public ApproxServiceService(ApproxServiceRepository approxServiceRepository) {
        this.approxServiceRepository = approxServiceRepository;

    }

    public ApproxService save(ApproxService service) {
        return approxServiceRepository.save(service);
    }

    public Optional<ApproxService> findByName(String name) {
        return approxServiceRepository.findByName(name);
    }

    public void deleteByName(String name) {
        approxServiceRepository.deleteByName(name);
    }
}
