package approximately.server.service;

import approximately.server.model.ApproxOrder;
import approximately.server.model.Product;
import approximately.server.model.User;
import approximately.server.repository.OrderRepository;
import approximately.server.repository.ProductRepository;
import approximately.server.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    ProductRepository productRepository;
    UserRepository userRepository;
    OrderRepository orderRepository;

    @Autowired
    public OrderService(ProductRepository productRepository, UserRepository userRepository, OrderRepository orderRepository) {
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.orderRepository = orderRepository;
    }

    public void placeOrder(ApproxOrder order, String userEmail) {
        Long orderValue = checkoutOrder(order);
        place(order, userEmail, orderValue);
    }

    private Long checkoutOrder(ApproxOrder order) {
        List<Product> products = order.getProducts();
        Long amount = 0L;
        for (Product product : products) {
            Optional<Product> p = productRepository.findById(product.getId());
            if (p.isPresent()) {
                amount += p.get().getPrice();
            }
        }
        return amount;
    }

    private void place(ApproxOrder order, String userEmail, Long orderValue) {
        User u = userRepository.findById(userEmail).get();
        if (orderValue <= u.getBalance()) {
            orderRepository.save(order);
            u.setBalance(u.getBalance() - orderValue);
        }
    }
}
