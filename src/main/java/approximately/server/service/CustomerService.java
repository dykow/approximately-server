package approximately.server.service;

import approximately.server.model.Address;
import approximately.server.model.ApproxService;
import approximately.server.model.Customer;
import approximately.server.repository.AddressRepository;
import approximately.server.repository.ApproxServiceRepository;
import approximately.server.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository, AddressRepository addressRepository, ApproxServiceRepository serviceRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }


    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    public ArrayList<Customer> findAllByName(String name) {
        return customerRepository.findAllByName(name);
    }

    public Optional<Customer> findByName(String name) {
        return customerRepository.findByName(name);
    }

    public void deleteByName(String name) {
        customerRepository.deleteByName(name);
    }
}
