#!/usr/bin/env bash
mkdir -p target/dependency
(cd target/dependency; jar -xf ../*.jar)
docker build . $1
docker-compose up -d